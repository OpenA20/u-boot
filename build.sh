#!/bin/bash
set -e

CHIP="sun7i"
PLATFORM=""
BOARD="evb"
MODULE=""

cpu_cores=`cat /proc/cpuinfo | grep "processor" | wc -l`
if [ ${cpu_cores} -le 8 ] ; then
    jobs=${cpu_cores}
else
    jobs=`expr ${cpu_cores} / 2`
fi

function mk_error()
{
    echo -e "\033[47;31mERROR: $*\033[0m"
}

function mk_warn()
{
    echo -e "\033[47;34mWARN: $*\033[0m"
}

function mk_info()
{
    echo -e "\033[47;30mINFO: $*\033[0m"
}

show_help()
{
	printf "\nbuild.sh - Top level build scritps\n"
	echo "Valid Options:"
	echo "  -h  Show help message"
	echo "  -c <chip> chip, e.g. sun7i"
	echo "  -p <platform> platform, e.g. dragonboard, android, linux"
	echo "  -b <board> board, e.g. evb"
	printf "  -m <module> module, e.g. clean,all \n\n"
}

if [ $(basename `pwd`) != "u-boot" ] ; then
    echo "Please run at the top directory of u-boot"
    exit 1
fi

while getopts hc:b:p:m: OPTION
do
	case $OPTION in
	h) show_help
	;;
	c) CHIP=$OPTARG
	;;
	b) BOARD=$OPTARG
	;;
	p) PLATFORM=$OPTARG
	;;
	m) MODULE=$OPTARG
	;;
	*) show_help
	;;
esac
done

if [ -z "$PLATFORM" ]; then
	show_help
	exit 1
fi

if [ "${CHIP}" = "${PLATFORM}" ] ; then
    PLATFORM="linux"
fi

if [ -z "$MODULE" ]; then
	MODULE="all"
fi

mk_info "----------------------------------------"
mk_info "Build u-boot ..."
mk_info "chip: $CHIP"
mk_info "platform: $PLATFORM"
mk_info "board: $BOARD"
mk_info "board: $MODULE"
mk_info "----------------------------------------"

tooldir="$(dirname `pwd`)/out/${CHIP}/${PLATFORM}/${BOARD}/buildroot/external-toolchain"
if [ -d ${tooldir} ] ; then
    if ! echo $PATH | grep -q "$tooldir" ; then
        export PATH=${tooldir}/bin:$PATH
    fi
else
	tooldir="$(dirname `pwd`)/out/${PLATFORM}/common/buildroot/external-toolchain"
  	if [ -d ${tooldir} ] ; then
    	if ! echo $PATH | grep -q "$tooldir" ; then
        	export PATH=${tooldir}/bin:$PATH
    	fi
    else
   		mk_error "Please build buildroot first"
    	exit 1
    fi
fi

case "$MODULE" in
    clean)
        make distclean CROSS_COMPILE=arm-linux-gnueabi-
        ;;
    *)
#       make distclean CROSS_COMPILE=arm-linux-gnueabi-
        make -j${jobs} ${CHIP} CROSS_COMPILE=arm-linux-gnueabi-

esac
