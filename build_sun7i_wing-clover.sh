#!/bin/bash

set -e

case "$1" in
    clean)
		./build.sh -c sun7i -p dragonboard -b wing-clover -m clean
        ;;
    *)
		./build.sh -c sun7i -p dragonboard -b wing-clover -m all
		;;
esac
